package qa.java.sol.library1;

import java.util.Comparator;

public class MemberNameSort implements Comparator {

	public int compare(Object o1, Object o2) {
		return ((Member)o1).getName().compareTo(((Member)o2).getName());
	}

}
