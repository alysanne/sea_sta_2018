package qa.java.sol.library1;

import java.util.*;

public class Library
{
    private Collection theMembers;

    public Library()
    {
    	  //theMembers = new LinkedList();
    	theMembers = new TreeSet(Member.NameSort);
    }

    public void addMember(String name, int age)
    {
    	  Member m = new Member(name, age);
        theMembers.add(m);
    }

    public String getMembers()
    {
    	StringBuffer sb = new StringBuffer(200);

        Iterator iter = theMembers.iterator();
        while (iter.hasNext())
        {
        	Member nextMember = (Member)iter.next();
            sb.append(nextMember.getDetails() + "\n");
        }

        return sb.toString();
    }

    public boolean removeMember(String name)
    {
    	boolean result = false;
    	Iterator iter = theMembers.iterator();
        while (iter.hasNext())
        {
        	Member nextMember = (Member)iter.next();
        	String memName = nextMember.getName();
        	if (name.equals(memName))
        	{
        		iter.remove();
                result = true;
            	break;
        	}
        }
        return result;
    }
}