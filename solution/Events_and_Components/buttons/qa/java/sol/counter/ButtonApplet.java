package qa.java.sol.counter;

import java.applet.Applet;
import java.awt.*;
import java.awt.event.*;

// ToDo:
//
// Declare the applet to be an ActionListener
// and make sure that the applet compiles correctly

public class ButtonApplet extends Applet
                     implements ActionListener
{
	private int count = 0; // the count

	//
	// ToDo:
	//
	// Define three instance variables to hold
	// references to two buttons and a text field

	private Button increment, decrement;
	private TextField text;


	public void init()
	{
		//
		// ToDo:
		//
		// Create the text field and add it to this Applet
		// using the add()command from the Container class
		//
        text = new TextField("0",3);
        add(text);

		//
		// ToDo:
		//
		//
		// Create two Buttons and add them to this Applet
		//
		increment = new Button("Increment");
		decrement = new Button("Decrement");

		add(increment);
		add(decrement);

		//
		// ToDo:
		//
		// Register this applet as the listener for
		// your two Buttons
		//
		increment.addActionListener(this);
		decrement.addActionListener(this);

	}

    public void actionPerformed(ActionEvent evt)
    {
        //
        // ToDo:
        //
        // Inside your actionPerformed() method:
        // use the getSource() method of the ActionEvent class
        // to discover which Button was pressed.
        //
        // Increment or decrement the count variable.
        //
        // Set the text in the TextField to (""+count)
        //

        if(evt.getSource() == increment)
        {
            count ++;
        }
        else if (evt.getSource() == decrement)
        {
            count --;
        }
        text.setText("" + count);
    }


}   //  ends Buttons
