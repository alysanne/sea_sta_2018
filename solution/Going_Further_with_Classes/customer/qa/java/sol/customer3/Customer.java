/* Class for employee */
package qa.java.sol.customer3;

public class Customer
{
	// Instance variables to hold customer's name and account number
	private String name;
    private int accountNumber;

	// Another instance variable to hold customer's status
	private char status;

	// A class variable for last-used account number
	private static int lastUsedAccountNumber = 1000;

	//
	// Constructor that takes a String to initialise the instance variable for the customer's
	// name, and sets the status automatically to 'A' as a default value
	//
    public Customer(String name)
    {
     	// make use of existing constructor
		this(name, 'A');
	}


	//
	// Second constructor takes a String and a char, the customer's name and status
	//
    public Customer(String name, char status)
    {
		this.name = name;
		if (status == 'A' || status == 'I'|| status == 'H') {
        	this.status = status;
		} else {
			this.status = 'H'; //default to on-hold if incorrect value received
		}
		accountNumber = ++lastUsedAccountNumber; //note the prefix form of the operator
	}


	//
	// The getName method returns the name
	//
    public String getName()
    {
		return name;
	}


	//
	// The getAccountNumber method returns the accountNumber
	//
    public int getAccountNumber()
    {
		return accountNumber;
	}

	//
	// The getStatus method returns the status
	//
    public char getStatus()
    {
		return status;
	}

	//
	// This changeDetails method changes the name
	//
    public void changeDetails(String name)
    {
		this.name = name;
	}


	//
	// This changeDetails method changes the name and status
	//
    public void changeDetails(String name, char status)
    {
		this.name = name;
		this.status = status;
	}


	//
	// The isHeld method returns true if the customer is status "H" for on-hold,
	// otherwise it returns false
	//
    public boolean isHeld()
    {
		return status == 'H';
	}


	//
	// The class method setLastUsedAccountNumber() resets the class variable
	//
	public static void setLastUsedAccountNumber(int newVal)
	{
		lastUsedAccountNumber = newVal;
	}

}
