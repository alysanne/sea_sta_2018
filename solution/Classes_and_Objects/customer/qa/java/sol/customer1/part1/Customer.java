/* Class for customer */
package qa.java.sol.customer1.part1;

public class Customer
{
	// Variables to hold customer's name and account number
	public String name;
    public int accountNumber;

	//
	// Constructor that takes a String and an int argument
	// to initialise the instance variables for the customer's name
	// and account number, respectively.
	//
    public Customer(String n, int a)
    {
		name = n;
		accountNumber  = a;
	}


}
